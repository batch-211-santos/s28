// CRUD Operations

/*

-CRUD is an acronym for: Create, Read, Update and Delete

-Create: to create or insert new record in the database

-Read: similar to search, it allows users to search and retrieve specific records

-Update: to modify existing records in the database

-Delete: to remove records from a database that is no longer needed


*/


// Create: Insert Documents

/*
-the mongo shell uses Javascript for its  syntax
-MongoDB deals with objects as it's structure for documentgs.
-We can create documents by providing objects into our methods

-Javascript Syntax:
	-object.object.method({object});

*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "876549321",
		email: "janedoe@gmail.com"
	}
})


//	INSERT MANY

/*
- Syntax:
	-db.collectionName:insertMany([{objectA}, {objectB} ])

*/

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		course: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		course: ["React", "Laravel", "Sass"]
		}
	]);


// READ: Find/Retrieve Documents

/*
-Syntax:
	-db.collectionName.find();

*/

db.users.find();

/*
	-db.collectioName.find({field:value})
*/

db.users.find({firstName: "Stephen"});

/*
FIND USING MULTIPLE PARAMETERS

	-db.collectionName.find({fieldA: valueA, fieldB: valueB});

*/

db.users.find({lastName: "Armstrong", age: 82});

// FIND + PRETTY METHOD

/*
-The pretty method allows us to be able to view the documents returned by our terminal in a "prettier" format.

-Syntax:
	-db.collectionName.find({field:value}).pretty();

*/

db.users.find({lastName: "Armstrong", age: 82}).pretty();



// Update: Edit a document

// UPDATE ONE: Updating a single document

/*
Syntax:
	-db.collectioName.updateOne({criteria}, {$set: {field:value}});

*/

// Example

db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.updateOne(
		{ firstName: "Test"},
		{
			$set:{
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact: {
					phone: "87654321",
					email: "bill@gmail.com"
				},
				courses: ["PHP", "Laravel", "HTML"],
				department: "Operations",
				status: "active"
			}
		}
	)

	db.users.find({lastName: "Gates", age: 65}).pretty();



db.users.updateMany(
		{department: "none"},
		{
			$set: {department: "HR"}
		}
	)


db.users.find().pretty();


db.users.insertOne({
	firstName: "Jane",
	lastName: "Dalisay",
	age: 31,
	contact: {
		phone: "876549321",
		email: "johndalisay@gmail.com"
	}
})



db.users.updateOne(
		{ firstName: "John"},
		{
			$set:{
				lastName: "Dela Cruz"
			}
		}
	)

db.users.find({firstName: "John"}).pretty();


// ReplaceOne replaces the whole document - 

db.users.replaceOne(
	{firstName: "Bill"},
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@rocketmail.com"
				},
		    courses: ["PHP", "Laravel", "HTML"],
		    department: "Operations",
	}
	);

db.users.find({firstName: "Bill"});



// Delete

db.users.insert({
	firstName: "test"
});


// Deleting a single document
	
-Syntax: 
	-db.collectionName.deleteOne({criteria})


db.users.deleteOne({
	firstName: "test"

});

db.users.find({firstName: "test"}).pretty();


// Deleting many documents

// Syntax:
	// -db.collectionName:deleteMany({criteria});



db.users.insertMany([
	{
		firstName: "Bill",
		lastName: "Magalang",
		age: 12,
	},
	{
		firstName: "Bill",
		lastName: "Batumbakal",
		age: 13,
	}
	]);


db.users.deleteMany({
	firstName: "Bill",
})

db.users.find({firstName: "Bill"}).pretty();



// DELETE ALL

// Syntax:
// 	-db.collectionName.deleteMany();

// it is good to practice soft deletion or archiving of our documents instead of deleting them or removing them from the system


// db.users.find({isActive: false});




// 	ADVANCED QUERIES

/*
-Retrieving data with complex data structures is also a good skill for any developer to have

- Real world examples of data can be as complex as having two or more layers of nested objects

-Learning to query these kinds of data is also essential to ensure that we are able to retrieve any informtaion that we would need in our application


*/

// Queary an embedded document
// -An embedded document are those types of documents that contain a document inside a document.

db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
}).pretty();

// Query on nested field]

db.users.find({
	"contact.email": "janedoe@gmail.com"
})

// Querying an Array with Exact Elements

db.users.find({courses: ["CSS", "Javascript", "Python"]}).pretty();

// Querying an Array without regard to order

db.users.find({courses: {$all: ["React", "Python"]}}).pretty();


// Querying an Embedded Array
db.users.insertOne({
	namearr: [
		{
			nameA: "Juan"
		},
		{
			nameB: "Tamad"
		}
	]
});

db.users.find({
	namearr:
	{
		nameA: "Juan"
	}

}).pretty();



/*

db.users.insertOne({
	"firstName": "John",
	"lastName": "Smith"
});


db.users.insertMany([
	{"firstName": "John", "lastName": "Doe"},
	{"firstName": "Jane", "lastName": "Doe"}
]);


db.users.find();


db.users.find({ "lastName": "Doe" });



db.users.updateOne(
{ 
	"_id": ObjectId("6345686f160dda97cf9cb07d")
},
{
	$set: {
		"email": "johnsmith@gmail.com"
	}
}
);


db.users.updateMany(
{ 
	"lastName": "Doe"
},
{
	$set: {
		"isAdmin": false
	}
}
);




db.users.updateMany(
{ 
	"lastName": "Doe"
},
{
	$set: {
		"isAdmin": false
	}
}
);


*/

